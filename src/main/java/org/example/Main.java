package org.example;

public class Main {
    public static void main(String[] args) {
        int x = 10;
        int y = 30;
        double k = (double) y / x;
        double q = (double) y/2;

        for (int row = 0; row <= x/3+1; row++) {
            for (int col = 0; col <= y; col++) {
                char c;
                if (col == q - k * row || col == q + k * row) {
                    c='*';
                } else if (row == 0 && col == q) {
                    c='*';
                } else {
                    c=' ';
                }
                System.out.print(c);
            }
            System.out.println();
        }

        for (int row = 0; row <= x; row++) {
            for (int col = 0; col <= y; col++) {
                char c;
                if (row == 0 || col == 0 || row == x || col == y || col == k*row ||col == y - k * row) {
                    c='*';
                } else {
                    c=' ';
                }
                System.out.print(c);
            }
            System.out.println();
        }
    }
}